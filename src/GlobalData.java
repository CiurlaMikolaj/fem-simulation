class GlobalData {
    double H = 0.100;
    double L = 0.100;
    double tInit = 100;
    double tau = 20;
    double dTau = 1;
    double tAmb = 1200;
    double alfa = 300;
    double c = 700;
    double k = 25;
    double ro = 7800;

    int nH = 31;
    int nL = 31;
    int nN = nH * nL;
    int nE = (nH - 1) * (nL - 1);

    GlobalData() {}
}