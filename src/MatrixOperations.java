class MatrixOperations {

    double[][] transposedVectorXVector(double[] m1, double[] m2){
        double[][] result = new double[m1.length][m2.length];
        for(int i = 0; i < m1.length; i++){
            for(int j = 0; j < m2.length; j++){
                result[i][j] = m1[i] * m2[j];
            }
        }

        return result;
    }

    double[][] matrixAddMatrix(double[][] m1, double[][] m2){
        double[][] result = new double[m1.length][m2.length];
            for (int i = 0; i < m1.length; i++) {
                for (int j = 0; j < m2.length; j++) {
                    result[i][j] = m1[i][j] + m2[i][j];
                }
            }

        return result;
    }

    double[][] matrixXScalar(double[][] m1, double scalar) {
        double[][] result = new double[m1.length][m1.length];
        for (int i = 0; i < m1.length; i++) {
            for (int j = 0; j < m1[0].length; j++) {
                result[i][j] = m1[i][j] * scalar;
            }
        }

        return result;
    }

    double[] transposedVectorXMatrix(double[] vector, double[][] matrix){
        double[] result = new double[matrix[0].length];

        for (int i = 0; i < matrix[0].length; i++) {
            for(int j = 0; j < vector.length; j++){
                double temp = 0;
                for (int k = 0; k < matrix.length; k++) {
                    temp += matrix[i][k] * vector[k];
                }
                result[i] = temp;
            }
        }

        return result;
    }

    private void matrixGaussian(double[][] a, int[] index){
        int n = index.length;
        var c = new double[n];

        // Initialize the index
        for (int i = 0; i < n; ++i)
            index[i] = i;

        // Find the rescaling factors, one from each row
        for (int i = 0; i < n; ++i) {
            double c1 = 0;
            for (int j = 0; j < n; ++j) {
                double c0 = Math.abs(a[i][j]);
                if (c0 > c1) c1 = c0;
            }
            c[i] = c1;
        }

        // Search the pivoting element from each column
        int k = 0;
        for (int j = 0; j < n - 1; ++j) {
            double pi1 = 0;
            for (int i = j; i < n; ++i) {
                double pi0 = Math.abs(a[index[i]][j]);
                pi0 /= c[index[i]];
                if (pi0 > pi1) {
                    pi1 = pi0;
                    k = i;
                }
            }

            // Interchange rows according to the pivoting order
            int itmp = index[j];
            index[j] = index[k];
            index[k] = itmp;
            for (int i = j + 1; i < n; ++i) {
                double pj = a[index[i]][j] / a[index[j]][j];

                // Record pivoting ratios below the diagonal
                a[index[i]][j] = pj;

                // Modify other elements accordingly
                for (int l = j + 1; l < n; ++l)
                    a[index[i]][l] -= pj * a[index[j]][l];
            }
        }
    }

    double[][] invertMatrix(double[][] matrix){
        //double[][] result = matrix;

        int h = matrix[0].length;
        int w = matrix.length;
        //assert h == w

        int size = w;
        var x = new double[size][size];
        var b = new double[size][size];
        var index = new int[size];
        for (int i=0; i<size; ++i)
            b[i][i] = 1;

        // Transform the matrix into an upper triangle
        matrixGaussian(matrix, index);

        // Update the matrix b[i][j] with the ratios stored
        for (int i=0; i<size-1; ++i)
            for (int j=i+1; j<size; ++j)
                for (int k=0; k<size; ++k)
                    b[index[j]][k]
                            -= matrix[index[j]][i]*b[index[i]][k];

        // Perform backward substitutions
        for (int i=0; i<size; ++i)
        {
            x[size-1][i] = b[index[size-1]][i]/matrix[index[size-1]][size-1];
            for (int j=size-2; j>=0; --j)
            {
                x[j][i] = b[index[j]][i];
                for (int k=j+1; k<size; ++k)
                {
                    x[j][i] -= matrix[index[j]][k]*x[k][i];
                }
                x[j][i] /= matrix[index[j]][j];
            }
        }
        return x;

        //return result;
    }

    double findMIN(double[] vector){
        double min = vector[0];

        for(int i = 0; i < vector.length; i++){
            double value = vector[i];
            if(value < min) min = value;
        }

        return min;
    }

    double findMAX(double[] vector){
        double max = vector[0];

        for(int i = 0; i < vector.length; i++){
            double value = vector[i];
            if(value > max) max = value;
        }

        return max;
    }

    String printMatrix(double[][] m){
        String result = "";
        for(int k = 0; k < m.length; k++){
            for(int l = 0; l < m[0].length; l++){
                result += String.format("[%8.5f]", m[k][l]);
            }
            result += "\n";
        }
        return result;
    }

}
