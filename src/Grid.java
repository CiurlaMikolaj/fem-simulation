import java.util.ArrayList;
import java.util.List;

class Grid{

    double[][] hGlobalMatrix, cGlobalMatrix;
    double[] pGlobalVector, t0Vector;

    private GlobalData globalData = new GlobalData();
    private MatrixOperations operations = new MatrixOperations();

    List<Element> elements;
    List<Node> nodes = new ArrayList<>();

    Grid(List<Element> elements) {
        this.elements = elements;
        for(Element e:elements){
            this.nodes.addAll(e.nodes);
        }

        hGlobalMatrix = new double[globalData.nN][globalData.nN];
        for(int i = 0; i < globalData.nN; i++){
            for(int j = 0; j < globalData.nN; j++) hGlobalMatrix[i][j] = 0;
        }

        cGlobalMatrix = new double[globalData.nN][globalData.nN];
        for(int i = 0; i < globalData.nN; i++){
            for(int j = 0; j < globalData.nN; j++) cGlobalMatrix[i][j] = 0;
        }

        pGlobalVector = new double[globalData.nN];
        for(int i = 0; i < globalData.nN; i++) pGlobalVector[i] = 0;

        aggregate();

        t0Vector = new double[globalData.nN];
        for(int i = 0; i < globalData.nN; i++) t0Vector[i] = globalData.tInit;
    }

    private void aggregate(){
        for(Element el : elements){
            int[] ID = new int[el.nodes.size()];
            for(int i = 0; i < el.nodes.size(); i++) ID[i] = el.nodes.get(i).id;

            for (int i = 0; i < el.nodes.size(); i++) {
                for (int j = 0; j < el.nodes.size(); j++) {
                    hGlobalMatrix[ID[i]-1][ID[j]-1] += el.hLocalMatrix[i][j] * globalData.k + el.hBCLocalMatrix[i][j] * globalData.alfa;
                    cGlobalMatrix[ID[i]-1][ID[j]-1] += el.cLocalMatrix[i][j] * globalData.c * globalData.ro;
                }

                pGlobalVector[ID[i]-1] += el.pVector[i] * globalData.alfa * globalData.tAmb;
            }
        }
    }

    void simulate(){
        double[][] cMatirxDTau = operations.matrixXScalar(cGlobalMatrix, 1/globalData.dTau);
        double[][] hAndCMatrix = operations.matrixAddMatrix(hGlobalMatrix, cMatirxDTau);
        double[][] hMatrixInverted = operations.invertMatrix(hAndCMatrix);

        double[] stepVector = operations.transposedVectorXMatrix(t0Vector, cMatirxDTau);
        for(int i = 0; i < stepVector.length; i++) stepVector[i] += pGlobalVector[i];
        //for(int i = 0; i < stepVector.length; i++)System.out.println(stepVector[i]+" ");

        for (double i = globalData.dTau; i <= globalData.tau; i += globalData.dTau) {

            double[] soughtTemps = operations.transposedVectorXMatrix(stepVector, hMatrixInverted);

            double min = operations.findMIN(soughtTemps);
            double max = operations.findMAX(soughtTemps);

            System.out.println("StepTime = "+ i +";\t Min temp = "+ min +";\t Max temp = "+ max +";");
            //System.out.println(operations.printMatrix(hAndCMatrix));
            //System.out.println(operations.printMatrix(hMatrixInverted));

            stepVector = operations.transposedVectorXMatrix(soughtTemps, cMatirxDTau);
            for(int j = 0; j < stepVector.length; j++) stepVector[j] += pGlobalVector[j];
        }
    }

}