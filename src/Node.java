public class Node {
    private static int idStatic = 1;
    int id;
    double x,y,t;
    boolean BC;

    Node(double x, double y, double t,boolean BC) {
        this.x = x;
        this.y = y;
        this.t = t;
        this.id = idStatic;
        this.BC = BC;

        idStatic++;
    }

    @Override
    public String toString() {
        return "[idNode = " + id +
                // ", x = " + x + ", y = " + y + ", t = " + t + ", BC = " + BC +
                "]";
    }
}