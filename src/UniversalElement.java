class UniversalElement {

    IntegrationPoint[] integrationPoints;
    int numberOfPoints;

    double[][] N_Matrix, dNdKsi_Matrix, dNdEta_Matrix;

    UniversalElement(IntegrationPoint[] integrationPoints) {
        this.integrationPoints = integrationPoints;
        numberOfPoints = integrationPoints.length;

        N_Matrix = new double[numberOfPoints][4];
        dNdKsi_Matrix = new double[numberOfPoints][4];
        dNdEta_Matrix = new double[numberOfPoints][4];

        calculateNmatrices();
    }

    private void calculateNmatrices() {
        for (int i = 0; i < numberOfPoints; i++) {

            for(int j = 0; j < numberOfPoints; j++) N_Matrix[i][j] = calculateNValues(integrationPoints[i])[j];

            for(int j = 0; j < numberOfPoints; j++) dNdKsi_Matrix[i][j] = calculateNdKsiValues(integrationPoints[i])[j];

            for(int j = 0; j < numberOfPoints; j++) dNdEta_Matrix[i][j] = calculateNdEtaValues(integrationPoints[i])[j];

        }
    }

    double[] calculateNValues(IntegrationPoint p){
        double[] result = new double[4];

        result[0] = 0.25 * (1 - p.ksi) * (1 - p.eta);
        result[1] = 0.25 * (1 + p.ksi) * (1 - p.eta);
        result[2] = 0.25 * (1 + p.ksi) * (1 + p.eta);
        result[3] = 0.25 * (1 - p.ksi) * (1 + p.eta);

        return result;
    }

    private double[] calculateNdKsiValues(IntegrationPoint p){
        double[] result = new double[4];

        result[0] = (-0.25) * (1 - p.eta);
        result[1] = 0.25 * (1 - p.eta);
        result[2] = 0.25 * (1 + p.eta);
        result[3] = (-0.25) * (1 + p.eta);

        return result;
    }

    private double[] calculateNdEtaValues(IntegrationPoint p){
        double[] result = new double[4];

        result[0] = (-0.25) * (1 - p.ksi);
        result[1] = (-0.25) * (1 + p.ksi);
        result[2] = 0.25 * (1 + p.ksi);
        result[3] = 0.25 * (1 - p.ksi);

        return result;
    }

    IntegrationPoint[] integrationPointsOfEdge(int edge){
        IntegrationPoint[] pointsOfEdge = new IntegrationPoint[(int)Math.round(Math.sqrt(numberOfPoints))];

        for(int i = 0; i<pointsOfEdge.length; i++){
            if(edge == 0){
                pointsOfEdge[i] = new IntegrationPoint(integrationPoints[i].ksi, -1,
                                    integrationPoints[i].weightKsi, integrationPoints[i].weightEta);
            }
            else if(edge == 1){
                pointsOfEdge[i] = new IntegrationPoint(1, integrationPoints[i*pointsOfEdge.length].eta,
                        integrationPoints[i*pointsOfEdge.length].weightKsi, integrationPoints[i*pointsOfEdge.length].weightEta);
            }
            else if(edge == 2){
                pointsOfEdge[i] = new IntegrationPoint(integrationPoints[i].ksi, 1,
                        integrationPoints[i].weightKsi, integrationPoints[i].weightEta);
            }
            else {
                pointsOfEdge[i] = new IntegrationPoint(-1, integrationPoints[i*pointsOfEdge.length].eta,
                        integrationPoints[i*pointsOfEdge.length].weightKsi, integrationPoints[i*pointsOfEdge.length].weightEta);
            }
        }

        return pointsOfEdge;
    }
}
