import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        GlobalData globalData = new GlobalData();

        //Creating nodes for grid:
        List<Node> nodes=new ArrayList<>();

        double dx = globalData.L/(globalData.nL-1);
        double dy = globalData.H/(globalData.nH-1);
        for(int j=1; j<=globalData.nL;j++){
            for(int i =1;i<=globalData.nH;i++){
                boolean BC = false;
                if(i == 1 || i == globalData.nH || j == 1 || j == globalData.nL){
                    BC = true;
                }
                nodes.add(new Node( (j-1) * dx, (i-1) * dy, globalData.tInit, BC));
            }
        }

        //Creating intergetion points for 2 point 2D ELement:
        IntegrationPoint [] tmp = new IntegrationPoint[4];

        tmp[0] = new IntegrationPoint((-1) / Math.sqrt(3), (-1) / Math.sqrt(3), 1, 1); //lewy dolny
        tmp[1] = new IntegrationPoint(1 / Math.sqrt(3), (-1) / Math.sqrt(3), 1, 1); //prawy dolny
        tmp[2] = new IntegrationPoint(1 / Math.sqrt(3), 1 / Math.sqrt(3), 1, 1); //prawy gorny
        tmp[3] = new IntegrationPoint((-1) / Math.sqrt(3), 1 / Math.sqrt(3), 1, 1); //lewy gorny

        //Creating 2 point 2D ELement:
        UniversalElement TwoPoint2DElement = new UniversalElement(tmp);

        //Creating Elements from nodes for grid:
        List<Element>elements = new ArrayList<>();

        int j = 0;
        for(int i = 0 ; i<globalData.nE;i++){
            if(i%(globalData.nH-1) == 0 && i != 0){
                j++;
            }
            elements.add(new Element(TwoPoint2DElement,
                    nodes.get(i + j),
                    nodes.get((i + j) + globalData.nH),
                    nodes.get((i + j) + globalData.nH+1),
                    nodes.get(i + j + 1)));
        }

        //Creating grid from elements:
        Grid grid = new Grid(elements);

        //System.out.println(grid.elements.get(0));

        /*
        MatrixOperations operations = new MatrixOperations();

        System.out.println("H + Hbc GLOBAL:");
        System.out.println(operations.printMatrix(grid.hGlobalMatrix));
        System.out.println("C GLOBAL:");
        System.out.println(operations.printMatrix(grid.cGlobalMatrix));
        System.out.println("P GLOBAL:");
        for(double el : grid.pGlobalVector){
            System.out.print("["+el+"] ");
        }
*/

        //Simulating process:
        grid.simulate();
    }
}
