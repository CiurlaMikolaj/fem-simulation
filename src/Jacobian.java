import java.util.List;

class Jacobian {
    double [][] invertedJacobian;
    double jacobianDET;

    Jacobian(UniversalElement universalElement, int integrationPoint, List<Node> nodes){

        double [][] jacobian = new double[2][2];

            //dX/dKsi
            jacobian[0][0] = universalElement.dNdKsi_Matrix[integrationPoint][0] * nodes.get(0).x +
                    universalElement.dNdKsi_Matrix[integrationPoint][1] * nodes.get(1).x +
                    universalElement.dNdKsi_Matrix[integrationPoint][2] * nodes.get(2).x +
                    universalElement.dNdKsi_Matrix[integrationPoint][3] * nodes.get(3).x;


            //dY/dKsi
            jacobian[0][1] = universalElement.dNdKsi_Matrix[integrationPoint][0] * nodes.get(0).y +
                    universalElement.dNdKsi_Matrix[integrationPoint][1] * nodes.get(1).y +
                    universalElement.dNdKsi_Matrix[integrationPoint][2] * nodes.get(2).y +
                    universalElement.dNdKsi_Matrix[integrationPoint][3] * nodes.get(3).y;


            // dX/dEta
            jacobian[1][0] = universalElement.dNdEta_Matrix[integrationPoint][0] * nodes.get(0).x +
                    universalElement.dNdEta_Matrix[integrationPoint][1] * nodes.get(1).x +
                    universalElement.dNdEta_Matrix[integrationPoint][2] * nodes.get(2).x +
                    universalElement.dNdEta_Matrix[integrationPoint][3] * nodes.get(3).x;


            // dY/dEta
            jacobian[1][1] = universalElement.dNdEta_Matrix[integrationPoint][0] * nodes.get(0).y +
                    universalElement.dNdEta_Matrix[integrationPoint][1] * nodes.get(1).y +
                    universalElement.dNdEta_Matrix[integrationPoint][2] * nodes.get(2).y +
                    universalElement.dNdEta_Matrix[integrationPoint][3] * nodes.get(3).y;


            jacobianDET = jacobian[0][0] * jacobian[1][1] - jacobian[0][1] * jacobian[1][0];

            invertedJacobian = new double[2][2];

            invertedJacobian[0][0] = jacobian[1][1];
            invertedJacobian[0][1] = jacobian[0][1] * (-1);
            invertedJacobian[1][0] = jacobian[1][0] * (-1);
            invertedJacobian[1][1] = jacobian[0][0];

    }
}
