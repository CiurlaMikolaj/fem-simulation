class IntegrationPoint {
    double ksi, eta, weightKsi, weightEta;

    IntegrationPoint(double ksi, double eta, double weightKsi, double weightEta) {
        this.ksi = ksi;
        this.eta = eta;
        this.weightKsi = weightKsi;
        this.weightEta = weightEta;
    }
}