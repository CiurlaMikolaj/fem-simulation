import java.util.Arrays;
import java.util.List;

public class Element {
    private static int idStatic = 1;
    private int id;

    List<Node>nodes;
    private UniversalElement universalElement;

    double[][] hLocalMatrix, hBCLocalMatrix, cLocalMatrix;
    double[] pVector;
    private Jacobian[] jacobians2D;

    private MatrixOperations matrixOperation = new MatrixOperations();

    Element(UniversalElement universalElement, Node ...nodes) {
        this.nodes = Arrays.asList(nodes);
        this.id = idStatic;
        this.universalElement = universalElement;
        idStatic++;

        hLocalMatrix = new double[4][4];

        hBCLocalMatrix = new double[4][4];
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++) hBCLocalMatrix[i][j] = 0;
        }

        cLocalMatrix = new double[4][4];

        pVector = new double[4];
        for(int i = 0; i < 4 ; i++) pVector[i] = 0;

        jacobians2D = new Jacobian[universalElement.numberOfPoints];

        calculateJacobians();
        calculateHLocal();
        calculateCLocal();
        calculateBoundryHandP();
    }

    private void calculateJacobians() {
        for (var i = 0; i < universalElement.numberOfPoints; i++) {
            jacobians2D[i] = new Jacobian(universalElement, i, nodes);
        }
    }

    private double calculateDnDx(int integrationPointIndex, int shapeFunIndex) {
        double result = (1 / jacobians2D[integrationPointIndex].jacobianDET * (jacobians2D[integrationPointIndex].invertedJacobian[0][0] * universalElement.dNdKsi_Matrix[integrationPointIndex][shapeFunIndex]
                + jacobians2D[integrationPointIndex].invertedJacobian[0][1] * universalElement.dNdEta_Matrix[integrationPointIndex][shapeFunIndex]));
        return result;
    }

    private double calculateDnDy(int integrationPointIndex, int shapeFunIndex) {
        double result = (1 / jacobians2D[integrationPointIndex].jacobianDET * (jacobians2D[integrationPointIndex].invertedJacobian[1][0] * universalElement.dNdKsi_Matrix[integrationPointIndex][shapeFunIndex]
                + jacobians2D[integrationPointIndex].invertedJacobian[1][1] * universalElement.dNdEta_Matrix[integrationPointIndex][shapeFunIndex]));
        return result;
    }

    private double[][] calculateSubHMatrix(int i) {
        double[] dNdX = new double[4];

        for(int j = 0; j < 4; j++){
            dNdX[j] = calculateDnDx(i, j);
        }

        double[] dNdY = new double[4];

        for(int j = 0; j < 4; j++){
            dNdY[j] = calculateDnDy(i, j);
        }

        double [][] dNdX2 = matrixOperation.transposedVectorXVector(dNdX, dNdX);
        double [][] dNdY2 = matrixOperation.transposedVectorXVector(dNdY, dNdY);

        return matrixOperation.matrixAddMatrix(dNdX2, dNdY2);
    }



    private void calculateHLocal() {
        double[][] result = new double[4][4];

        for (int i = 0; i < universalElement.numberOfPoints; i++) {
            double tmp = universalElement.integrationPoints[i].weightKsi * universalElement.integrationPoints[i].weightEta * jacobians2D[i].jacobianDET;
            double[][] subHMatrix =  matrixOperation.matrixXScalar(calculateSubHMatrix(i), tmp);

            result = matrixOperation.matrixAddMatrix(result, subHMatrix);
        }

        hLocalMatrix = result;
    }

    private void calculateCLocal() {
        double[][] result = new double[4][4];

        for (int i = 0; i < universalElement.numberOfPoints; i++) {
            double [][] N2 = matrixOperation.transposedVectorXVector(universalElement.N_Matrix[i], universalElement.N_Matrix[i]);
            double tmp = universalElement.integrationPoints[i].weightKsi * universalElement.integrationPoints[i].weightEta * jacobians2D[i].jacobianDET;
            double[][] subCMatrix =  matrixOperation.matrixXScalar(N2, tmp);

            result = matrixOperation.matrixAddMatrix(result, subCMatrix);
        }

        cLocalMatrix = result;
    }

    private void calculateBoundryHandP(){

        for(int i = 0; i < nodes.size(); i++){

            if(nodes.get(i).BC && nodes.get((i+1)%nodes.size()).BC){
                double jacobianDET1D = (Math.sqrt(Math.pow((nodes.get((i+1)%nodes.size()).x - nodes.get(i).x),2)
                                        + Math.pow((nodes.get((i+1)%nodes.size()).y - nodes.get(i).y),2)))/2;

                IntegrationPoint[] pointsOfEdge = universalElement.integrationPointsOfEdge(i);

                for(int j = 0; j < pointsOfEdge.length; j++){
                    //Calculating H_BC Local:
                    double[] N = universalElement.calculateNValues(pointsOfEdge[j]);
                    double[][] N2 = matrixOperation.transposedVectorXVector(N, N);
                    double tmpH = pointsOfEdge[j].weightEta * pointsOfEdge[j].weightKsi * jacobianDET1D;
                    double [][] subHbcMatrix = matrixOperation.matrixXScalar(N2, tmpH);
                    hBCLocalMatrix = matrixOperation.matrixAddMatrix(hBCLocalMatrix, subHbcMatrix);

                    //Calculating P vector:
                    double tmpP = pointsOfEdge[j].weightEta * pointsOfEdge[j].weightKsi * jacobianDET1D;
                    double[] subPVector = new double[N.length];
                    for(int k = 0; k < subPVector.length; k++) {
                        subPVector[k] = N[k] * tmpP;
                        pVector[k] += subPVector[k];
                    }
                }
            }
        }
    }

    @Override
    public String toString(){
        String val = "\nElement ID = " + id + ", Nodes: \n{";
        for(Node n:nodes){
            val = val.concat("\n\tid = " +n.id +
                    "\n\tx = " +n.x + "\n\ty = " +n.y + "\n\tBC = " +n.BC +
                    "\n");
        }
        val += "}\n";
        MatrixOperations operation = new MatrixOperations();
        val += "H Local:\n";
        val += operation.printMatrix(hLocalMatrix);
        val += "H_BC local:\n";
        val += operation.printMatrix(hBCLocalMatrix);
        val += "C Local:\n";
        val += operation.printMatrix(cLocalMatrix);
        val += "P vector:\n";
        for(double el:pVector){
            val = val.concat(String.format("[%8.5f]", el));
        }
        return val;
    }
}